import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state={
      value:JSON.parse(localStorage.getItem("value")) || [] 
    }
  }
  add(){
    this.state.value.push(this.state.task)
    localStorage.setItem("value",JSON.stringify(this.state.value))
    this.setState({state:this.state})
  }
  render() {
    var value = localStorage.getItem("value")
    value = JSON.parse(value)
    console.log("#####",this.state)
    return (
      <div>
        <input
        onChange={(event)=>this.setState({task:event.target.value})}
        />
        <button onClick={this.add.bind(this)}>Add</button>
        {value && <ul>{value.map((data,index)=>
          <li key={index}>{data}</li>
        )}</ul>}
      </div>
    );
  }
}

export default App;
